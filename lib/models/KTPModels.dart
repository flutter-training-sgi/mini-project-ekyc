// To parse this JSON data, do
//
//     final ktpModels = ktpModelsFromJson(jsonString);

import 'dart:convert';
import 'dart:io';

import 'package:dio/dio.dart';
import 'package:e_kyc_mini_project/constants.dart';

KtpModels ktpModelsFromJson(String str) => KtpModels.fromJson(json.decode(str));

String ktpModelsToJson(KtpModels data) => json.encode(data.toJson());
Dio dio = Dio();
Future<KtpModels> recognizeKTP(File file) async {
  String fileName = file.path.split('/').last;
  FormData formData = FormData.fromMap({
    "key": "16b216754623f32714dd0c5bee56f87aa83cfd335d6ca415ce06c8886e977c4e",
    "image": await MultipartFile.fromFile(file.path, filename: fileName),
  });
  try {
    Response response = await dio.post("${hostKTP}", data: formData);
    if (response.statusCode == 200) {
      return KtpModels.fromJson(response.data);
    } else {
      return null;
    }
  } catch (e) {
    print(e);
    return null;
  }
}

Future<Response> submitKTP(
    KtpModels ktpModels,
    String userId,
    String nik,
    String nama,
    String tempatLahir,
    String tanggalLahir,
    String alamat,
    String rt,
    String desa,
    String kecamatan,
    String kota,
    String provinsi,
    String agama,
    String status,
    String pekerjaan) async {
  try {
    var body = {
      "nik": nik,
      "nama": nama,
      "tempatLahir": tempatLahir,
      "tanggalLahir": tanggalLahir,
      "alamat": alamat,
      "rt": rt,
      "desa": desa,
      "kecamatan": kecamatan,
      "kota": kota,
      "provinsi": provinsi,
      "agama": agama,
      "status": status,
      "pekerjaan": pekerjaan,
      "nik_scan": ktpModels.read.nik,
      "nama_scan": ktpModels.read.nama,
      "tempatLahir_scan": ktpModels.read.tempatLahir,
      "tanggalLahir_scan": ktpModels.read.tanggalLahir,
      "alamat_scan": ktpModels.read.alamat,
      "rt_scan": ktpModels.read.rtRw,
      "desa_scan": ktpModels.read.kelurahanDesa,
      "kecamatan_scan": ktpModels.read.kecamatan,
      "kota_scan": ktpModels.read.kotaKabupaten,
      "provinsi_scan": ktpModels.read.provinsi,
      "agama_scan": ktpModels.read.agama,
      "status_scan": ktpModels.read.statusPerkawinan,
      "pekerjaan_scan": ktpModels.read.pekerjaan,
      "user_id": userId
    };

    Response response =
        await dio.post("${host}/api/user_benef_ktp/create", data: body);
    if (response.statusCode == 200) {
      return response;
    } else {
      return null;
    }
  } catch (e) {
    print(e);
    return null;
  }
}

class KtpModels {
  KtpModels({
    this.read,
    this.status,
  });

  Read read;
  String status;

  factory KtpModels.fromJson(Map<String, dynamic> json) => KtpModels(
        read: Read.fromJson(json["read"]),
        status: json["status"],
      );

  Map<String, dynamic> toJson() => {
        "read": read.toJson(),
        "status": status,
      };
}

class Read {
  Read({
    this.agama,
    this.alamat,
    this.berlakuHingga,
    this.golonganDarah,
    this.jenisKelamin,
    this.kecamatan,
    this.kelurahanDesa,
    this.kewarganegaraan,
    this.kotaKabupaten,
    this.nama,
    this.nik,
    this.pekerjaan,
    this.provinsi,
    this.rtRw,
    this.statusPerkawinan,
    this.tanggalLahir,
    this.tempatLahir,
  });

  String agama;
  String alamat;
  String berlakuHingga;
  String golonganDarah;
  String jenisKelamin;
  String kecamatan;
  String kelurahanDesa;
  String kewarganegaraan;
  String kotaKabupaten;
  String nama;
  String nik;
  String pekerjaan;
  String provinsi;
  String rtRw;
  String statusPerkawinan;
  String tanggalLahir;
  String tempatLahir;

  factory Read.fromJson(Map<String, dynamic> json) => Read(
        agama: json["agama"],
        alamat: json["alamat"],
        berlakuHingga: json["berlakuHingga"],
        golonganDarah: json["golonganDarah"],
        jenisKelamin: json["jenisKelamin"],
        kecamatan: json["kecamatan"],
        kelurahanDesa: json["kelurahanDesa"],
        kewarganegaraan: json["kewarganegaraan"],
        kotaKabupaten: json["kotaKabupaten"],
        nama: json["nama"],
        nik: json["nik"],
        pekerjaan: json["pekerjaan"],
        provinsi: json["provinsi"],
        rtRw: json["rtRw"],
        statusPerkawinan: json["statusPerkawinan"],
        tanggalLahir: json["tanggalLahir"],
        tempatLahir: json["tempatLahir"],
      );

  Map<String, dynamic> toJson() => {
        "agama": agama,
        "alamat": alamat,
        "berlakuHingga": berlakuHingga,
        "golonganDarah": golonganDarah,
        "jenisKelamin": jenisKelamin,
        "kecamatan": kecamatan,
        "kelurahanDesa": kelurahanDesa,
        "kewarganegaraan": kewarganegaraan,
        "kotaKabupaten": kotaKabupaten,
        "nama": nama,
        "nik": nik,
        "pekerjaan": pekerjaan,
        "provinsi": provinsi,
        "rtRw": rtRw,
        "statusPerkawinan": statusPerkawinan,
        "tanggalLahir": tanggalLahir,
        "tempatLahir": tempatLahir,
      };
}
