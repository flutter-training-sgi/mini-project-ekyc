import 'dart:convert';
import 'dart:io';

import 'package:dio/dio.dart';
import 'package:e_kyc_mini_project/constants.dart';
import 'package:e_kyc_mini_project/models/faceRecognizeAddModel.dart';

UserBenefModel userBenefModelFromJson(String str) =>
    UserBenefModel.fromJson(json.decode(str));

String userBenefModelToJson(UserBenefModel data) => json.encode(data.toJson());

Dio dio = Dio();
Future<Response> registerUserBenef(
    String name, String gender, String birthday) async {
  try {
    var body = {
      "name": name,
      "gender": gender,
      "birthday": birthday,
      "kader_uid": "ef953fb9-eebc-4586-be4a-f54fbadb0a54",
      "program_uid": "8090012b-bbbf-4a6f-8d3c-58de37d10d75"
    };

    Response response =
        await dio.post("${host}/api/user_benef/create", data: body);
    if (response.statusCode == 200) {
      return response;
    }
  } catch (e) {
    print(e);
    return e;
  }
}

Future<Response> updatePin(String id, String pin) async {
  try {
    var body = {"pin": pin};
    print("${host}/api/user_benef/${id}/setpin");
    Response response =
        await dio.put("${host}/api/user_benef/${id}/setpin", data: body);
    if (response.statusCode == 200) {
      return response;
    }
  } catch (e) {
    print(e);
    return e;
  }
}

Future<Response> insertUserFace(
    String faceId, String userId, String source) async {
  try {
    var body = {
      "kader_uid": "ef953fb9-eebc-4586-be4a-f54fbadb0a54",
      "user_id": "${userId}",
      "aws_faceid": "${faceId}",
      "photo_source": "${source}",
      "active_status": 1
    };
    print("${host}/api/user_face");
    print(jsonEncode(body));
    Response response =
        await dio.post("${host}/api/user_face/create", data: body);
    if (response.statusCode == 200) {
      return response;
    }
  } catch (e) {
    print(e);
    return e;
  }
}

Future<FaceRecognizeAddModel> uploadFace(File file, String userId) async {
  String fileName = file.path.split('/').last;
  FormData formData = FormData.fromMap({
    "file": await MultipartFile.fromFile(file.path, filename: fileName),
  });
  try {
    List<int> imageBytes = file.readAsBytesSync();
    var data = {"photo": base64Encode(imageBytes), "id_user": userId};
    print("UPLOAD");
    print(base64Encode(imageBytes));
    Response response =
        await dio.post("${hostFace}/index-new-face", data: data);
    print(response.statusCode);
    print(jsonEncode(response.data));
    if (response.statusCode == 200) {
      return FaceRecognizeAddModel.fromJson(response.data);
    } else {
      return null;
    }
  } catch (e) {
    print(e);
    return null;
  }
}

class UserBenefModel {
  UserBenefModel({
    this.id,
    this.name,
    this.gender,
    this.birthday,
    this.face1,
    this.face2,
    this.hashedpin,
    this.verifiedstatus,
    this.activestatus,
    this.kaderUid,
    this.programUid,
    this.updatedAt,
    this.createdAt,
  });

  String id;
  String name;
  String gender;
  DateTime birthday;
  String face1;
  String face2;
  String hashedpin;
  int verifiedstatus;
  int activestatus;
  String kaderUid;
  String programUid;
  DateTime updatedAt;
  DateTime createdAt;

  factory UserBenefModel.fromJson(Map<String, dynamic> json) => UserBenefModel(
        id: json["id"],
        name: json["name"],
        gender: json["gender"],
        birthday: DateTime.parse(json["birthday"]),
        face1: json["face_1"],
        face2: json["face_2"],
        hashedpin: json["hashedpin"],
        verifiedstatus: json["verifiedstatus"],
        activestatus: json["activestatus"],
        kaderUid: json["kader_uid"],
        programUid: json["program_uid"],
        updatedAt: DateTime.parse(json["updatedAt"]),
        createdAt: DateTime.parse(json["createdAt"]),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "gender": gender,
        "birthday":
            "${birthday.year.toString().padLeft(4, '0')}-${birthday.month.toString().padLeft(2, '0')}-${birthday.day.toString().padLeft(2, '0')}",
        "face_1": face1,
        "face_2": face2,
        "hashedpin": hashedpin,
        "verifiedstatus": verifiedstatus,
        "activestatus": activestatus,
        "kader_uid": kaderUid,
        "program_uid": programUid,
        "updatedAt": updatedAt.toIso8601String(),
        "createdAt": createdAt.toIso8601String(),
      };
}
