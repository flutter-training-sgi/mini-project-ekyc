// To parse this JSON data, do
//
//     final faceRecognizeAddModel = faceRecognizeAddModelFromJson(jsonString);

import 'dart:convert';

FaceRecognizeAddModel faceRecognizeAddModelFromJson(String str) =>
    FaceRecognizeAddModel.fromJson(json.decode(str));

String faceRecognizeAddModelToJson(FaceRecognizeAddModel data) =>
    json.encode(data.toJson());

class FaceRecognizeAddModel {
  FaceRecognizeAddModel({
    this.found,
    this.resultAws,
  });

  bool found;
  ResultAws resultAws;

  factory FaceRecognizeAddModel.fromJson(Map<String, dynamic> json) =>
      FaceRecognizeAddModel(
        found: json["found"],
        resultAws: ResultAws.fromJson(json["resultAWS"]),
      );

  Map<String, dynamic> toJson() => {
        "found": found,
        "resultAWS": resultAws.toJson(),
      };
}

class ResultAws {
  ResultAws({
    this.faceId,
    this.boundingBox,
    this.imageId,
    this.externalImageId,
    this.confidence,
  });

  String faceId;
  BoundingBox boundingBox;
  String imageId;
  String externalImageId;
  double confidence;

  factory ResultAws.fromJson(Map<String, dynamic> json) => ResultAws(
        faceId: json["FaceId"],
        boundingBox: BoundingBox.fromJson(json["BoundingBox"]),
        imageId: json["ImageId"],
        externalImageId: json["ExternalImageId"],
        confidence: json["Confidence"],
      );

  Map<String, dynamic> toJson() => {
        "FaceId": faceId,
        "BoundingBox": boundingBox.toJson(),
        "ImageId": imageId,
        "ExternalImageId": externalImageId,
        "Confidence": confidence,
      };
}

class BoundingBox {
  BoundingBox({
    this.width,
    this.height,
    this.left,
    this.top,
  });

  double width;
  double height;
  double left;
  double top;

  factory BoundingBox.fromJson(Map<String, dynamic> json) => BoundingBox(
        width: json["Width"].toDouble(),
        height: json["Height"].toDouble(),
        left: json["Left"].toDouble(),
        top: json["Top"].toDouble(),
      );

  Map<String, dynamic> toJson() => {
        "Width": width,
        "Height": height,
        "Left": left,
        "Top": top,
      };
}
