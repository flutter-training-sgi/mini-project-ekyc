// To parse this JSON data, do
//
//     final programDocumentModel = programDocumentModelFromJson(jsonString);

import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:e_kyc_mini_project/constants.dart';

ProgramDocumentModel programDocumentModelFromJson(String str) =>
    ProgramDocumentModel.fromJson(json.decode(str));

String programDocumentModelToJson(ProgramDocumentModel data) =>
    json.encode(data.toJson());

Dio dio = Dio();
Future<ProgramDocumentModel> getProgramDocument(String programId) async {
  try {
    var url = "${host}/api/program_document/program/${programId}";
    print(url);
    Response response = await dio.get(url);
    if (response.statusCode == 200) {
      print(jsonEncode(response.data));
      return ProgramDocumentModel.fromJson(response.data);
    }
  } catch (e) {
    print("error");
    print(e);
    return null;
  }
}

class ProgramDocumentModel {
  ProgramDocumentModel({
    this.records,
  });

  List<Record> records;

  factory ProgramDocumentModel.fromJson(Map<String, dynamic> json) =>
      ProgramDocumentModel(
        records:
            List<Record>.from(json["records"].map((x) => Record.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "records": List<dynamic>.from(records.map((x) => x.toJson())),
      };
}

class Record {
  Record({
    this.id,
    this.programUid,
    this.documentUid,
    this.type,
    this.document,
  });

  String id;
  String programUid;
  String documentUid;
  String type;
  Document document;

  factory Record.fromJson(Map<String, dynamic> json) => Record(
        id: json["id"],
        programUid: json["program_uid"],
        documentUid: json["document_uid"],
        type: json["type"],
        document: Document.fromJson(json["document"]),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "program_uid": programUid,
        "document_uid": documentUid,
        "type": type,
        "document": document.toJson(),
      };
}

class Document {
  Document({
    this.id,
    this.documentName,
    this.documentType,
    this.createdAt,
    this.updatedAt,
  });

  String id;
  String documentName;
  String documentType;
  DateTime createdAt;
  DateTime updatedAt;

  factory Document.fromJson(Map<String, dynamic> json) => Document(
        id: json["id"],
        documentName: json["document_name"],
        documentType: json["document_type"],
        createdAt: DateTime.parse(json["createdAt"]),
        updatedAt: DateTime.parse(json["updatedAt"]),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "document_name": documentName,
        "document_type": documentType,
        "createdAt": createdAt.toIso8601String(),
        "updatedAt": updatedAt.toIso8601String(),
      };
}
