import 'dart:convert';

import 'package:e_kyc_mini_project/models/programDocumentModels.dart';
import 'package:e_kyc_mini_project/pages/scan_card.dart';
import 'package:e_kyc_mini_project/widget/AppBar.dart';
import 'package:flutter/material.dart';

class ListDocument extends StatefulWidget {
  final String programId;
  final String userId;
  final bool isAfterScan;
  ListDocument(this.programId, this.userId, {this.isAfterScan = false});

  @override
  _ListDocumentState createState() => _ListDocumentState();
}

class _ListDocumentState extends State<ListDocument> {
  late ProgramDocumentModel programDocumentModel;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBarWidget(
        title: "Data Pelengkap",
      ),
      body: Container(
        padding: EdgeInsets.all(10.0),
        child: FutureBuilder(
          future: getProgramDocument("993900b3-850a-46b3-9c7c-7ebf9c7d773d"),
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              print(jsonEncode(snapshot.data));
              programDocumentModel = snapshot.data!;
            }
            return snapshot.hasData
                ? Container(
                    child: ListView.builder(
                      itemCount: programDocumentModel.records.length,
                      itemBuilder: (context, int index) {
                        return InkWell(
                          onTap: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => ScanCard(
                                          documentName:
                                              "${programDocumentModel.records.elementAt(index).document.documentName}",
                                          userId: "${widget.userId}",
                                        )));
                          },
                          child: Container(
                            child: IgnorePointer(
                              child: TextField(
                                decoration: InputDecoration(
                                    labelText:
                                        "${programDocumentModel.records.elementAt(index).document.documentName}",
                                    suffixIcon: Icon(Icons.navigate_next)),
                              ),
                            ),
                          ),
                        );
                      },
                    ),
                  )
                : Center(child: CircularProgressIndicator());
          },
        ),
      ),
    );
  }
}
