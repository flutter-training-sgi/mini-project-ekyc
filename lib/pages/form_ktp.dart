import 'dart:io';

import 'package:dio/dio.dart';
import 'package:e_kyc_mini_project/models/KTPModels.dart';
import 'package:e_kyc_mini_project/pages/home.dart';
import 'package:e_kyc_mini_project/widget/AppBar.dart';
import 'package:flutter/material.dart';

class formKTP extends StatefulWidget {
  final KtpModels ktpModels;
  final File file;
  final String userId;
  final String programId;

  const formKTP(this.ktpModels,  this.file, this.userId, this.programId);

  @override
  State<formKTP> createState() => _formKTPState();
}

class _formKTPState extends State<formKTP> {
  TextEditingController _NIKController = TextEditingController();

  TextEditingController _nameController = TextEditingController();
  TextEditingController _tempatLahirController = TextEditingController();
  TextEditingController _tanggalLahirController = TextEditingController();
  TextEditingController _jenisKelaminController = TextEditingController();
  TextEditingController _alamatController = TextEditingController();
  TextEditingController _rtController = TextEditingController();
  TextEditingController _kelurahanController = TextEditingController();
  TextEditingController _kecamatanController = TextEditingController();
  TextEditingController _kotaController = TextEditingController();
  TextEditingController _provinsiController = TextEditingController();
  TextEditingController _agamaController = TextEditingController();
  TextEditingController _statusController = TextEditingController();
  TextEditingController _pekerjaanController = TextEditingController();
  TextEditingController _wargaNegaraController = TextEditingController();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _NIKController.text = "${widget.ktpModels.read.nik}";
    _nameController.text = "${widget.ktpModels.read.nama}";
    _tempatLahirController.text = "${widget.ktpModels.read.tempatLahir}";
    _tanggalLahirController.text = "${widget.ktpModels.read.tanggalLahir}";
    _jenisKelaminController.text = "${widget.ktpModels.read.jenisKelamin}";
    _alamatController.text = "${widget.ktpModels.read.alamat}";
    _rtController.text = "${widget.ktpModels.read.rtRw}";
    _kelurahanController.text = "${widget.ktpModels.read.kelurahanDesa}";
    _kecamatanController.text = "${widget.ktpModels.read.kecamatan}";
    _kotaController.text = "${widget.ktpModels.read.kotaKabupaten}";

    _provinsiController.text = "${widget.ktpModels.read.provinsi}";
    _agamaController.text = "${widget.ktpModels.read.agama}";
    _statusController.text = "${widget.ktpModels.read.statusPerkawinan}";
    _pekerjaanController.text = "${widget.ktpModels.read.pekerjaan}";
    _wargaNegaraController.text = "${widget.ktpModels.read.kewarganegaraan}";
  }

  submitData() async {
    Response response = await submitKTP(
        widget.ktpModels,
        widget.userId,
        _NIKController.text,
        _nameController.text,
        _tempatLahirController.text,
        _tanggalLahirController.text,
        _alamatController.text,
        _rtController.text,
        _kelurahanController.text,
        _kecamatanController.text,
        _kotaController.text,
        _provinsiController.text,
        _agamaController.text,
        _statusController.text,
        _pekerjaanController.text);
    if (response.statusCode == 200) {
      Navigator.of(context).pushAndRemoveUntil(
          MaterialPageRoute(builder: (ctx) => Home()),
          (Route<dynamic> route) => false);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBarWidget(
        title: "KTP",
      ),
      body: SingleChildScrollView(
        child: Container(
          padding: EdgeInsets.all(10.0),
          child: Column(
            children: [
              TextField(
                controller: _NIKController,
                decoration: InputDecoration(labelText: "NIK"),
              ),
              TextField(
                controller: _nameController,
                decoration: InputDecoration(labelText: "Nama"),
              ),
              TextField(
                controller: _tempatLahirController,
                decoration: InputDecoration(labelText: "Tempat Lahir"),
              ),
              TextField(
                controller: _tanggalLahirController,
                decoration: InputDecoration(labelText: "Tanggal Lahir"),
              ),
              TextField(
                controller: _jenisKelaminController,
                decoration: InputDecoration(labelText: "Jenis Kelamin"),
              ),
              TextField(
                controller: _alamatController,
                decoration: InputDecoration(labelText: "Alamat"),
              ),
              TextField(
                controller: _rtController,
                decoration: InputDecoration(labelText: "RT/RW"),
              ),
              TextField(
                controller: _kelurahanController,
                decoration: InputDecoration(labelText: "Kel/Desa"),
              ),
              TextField(
                controller: _kecamatanController,
                decoration: InputDecoration(labelText: "Kecamatan"),
              ),
              TextField(
                controller: _kotaController,
                decoration: InputDecoration(labelText: "Kota"),
              ),
              TextField(
                controller: _provinsiController,
                decoration: InputDecoration(labelText: "Provinsi"),
              ),
              TextField(
                controller: _agamaController,
                decoration: InputDecoration(labelText: "Agama"),
              ),
              TextField(
                controller: _statusController,
                decoration: InputDecoration(labelText: "Status Perkawinan"),
              ),
              TextField(
                controller: _pekerjaanController,
                decoration: InputDecoration(labelText: "Pekerjaan"),
              ),
              // TextField(
              //   controller: _wargaNegaraController,
              //   decoration: InputDecoration(labelText: "Kewarganegaraan"),
              // ),
              SizedBox(
                height: 20,
              ),
              ButtonTheme(
                minWidth: MediaQuery.of(context).size.width * 0.8,
                height: 50.0,
                shape: RoundedRectangleBorder(),
                buttonColor: Color.fromRGBO(35, 59, 123, 1),
                child: ElevatedButton(
                  onPressed: () {
                    submitData();
                  },
                  child: Text(
                    "Submit",
                    style: TextStyle(color: Colors.white, fontSize: 16.0),
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
