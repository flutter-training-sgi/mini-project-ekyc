import 'package:e_kyc_mini_project/pages/account_page.dart';
import 'package:e_kyc_mini_project/pages/form_register_page.dart';
import 'package:e_kyc_mini_project/pages/home_page.dart';
import 'package:e_kyc_mini_project/pages/notification_page.dart';
import 'package:e_kyc_mini_project/pages/report_page.dart';
import 'package:flutter/material.dart';

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> with SingleTickerProviderStateMixin {
  // Properties & Variables needed

  int currentTab = 0; // to keep track of active tab index
  final List<Widget> screens = [
    Homepage(),
    ReportPage(),
    NotificationPage(),
    AccountPage(),
  ]; // to store nested tabs
  final PageStorageBucket bucket = PageStorageBucket();
  Widget currentScreen = Homepage(); // Our first view in viewport

  double getRadiansFromDegree(double degree) {
    double unitRadian = 57.295779513;
    return degree / unitRadian;
  }

  AnimationController? animationController;
  Animation? degOneTranslationAnimation,
            degTwoTranslationAnimation,
            degThreeTranslationAnimation;
  Animation? rotationAnimation;

  @override
  void initState() {
    animationController =
        AnimationController(vsync: this, duration: Duration(milliseconds: 250));
    degOneTranslationAnimation =
        Tween(begin: 0.0, end: 1.0).animate(animationController!);
    rotationAnimation = Tween(begin: 180.0, end: 0.0).animate(
        CurvedAnimation(parent: animationController!, curve: Curves.easeOut));
    super.initState();
    animationController?.addListener(() {
      setState(() {});
    });
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        elevation: 0.0,
        title: Row(
          children: [
            Image.asset("assets/logo.png"),
            Text(
              "duithape v0.0.1",
              style: TextStyle(color: Colors.black),
            )
          ],
        ),
      ),
      body: PageStorage(
        child: currentScreen,
        bucket: bucket,
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(
          Icons.add,
          size: 35.0,
        ),
        onPressed: () {
          Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => FormRegisterPage()),
          );
        },
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
      // bottomNavigationBar: BottomAppBar(
      //   shape: CircularNotchedRectangle(),
      //   notchMargin: 10,
      //   child: Container(
      //     height: 60,
      //     child: Row(
      //       mainAxisAlignment: MainAxisAlignment.spaceBetween,
      //       children: <Widget>[
      //         Row(
      //           crossAxisAlignment: CrossAxisAlignment.start,
      //           children: <Widget>[
      //             MaterialButton(
      //               minWidth: 40,
      //               onPressed: () {
      //                 setState(() {
      //                   currentScreen =
      //                       Homepage(); // if user taps on this dashboard tab will be active
      //                   currentTab = 0;
      //                 });
      //               },
      //               child: Column(
      //                 mainAxisAlignment: MainAxisAlignment.center,
      //                 children: <Widget>[
      //                   Icon(
      //                     Icons.dashboard,
      //                     color: currentTab == 0 ? Colors.blue : Colors.grey,
      //                   ),
      //                   Text(
      //                     'Home',
      //                     style: TextStyle(
      //                       color: currentTab == 0 ? Colors.blue : Colors.grey,
      //                     ),
      //                   ),
      //                 ],
      //               ),
      //             ),
      //             MaterialButton(
      //               minWidth: 40,
      //               onPressed: () {
      //                 setState(() {
      //                   currentScreen =
      //                       ReportPage(); // if user taps on this dashboard tab will be active
      //                   currentTab = 1;
      //                 });
      //               },
      //               child: Column(
      //                 mainAxisAlignment: MainAxisAlignment.center,
      //                 children: <Widget>[
      //                   Icon(
      //                     Icons.chat,
      //                     color: currentTab == 1 ? Colors.blue : Colors.grey,
      //                   ),
      //                   Text(
      //                     'Laporan',
      //                     style: TextStyle(
      //                       color: currentTab == 1 ? Colors.blue : Colors.grey,
      //                     ),
      //                   ),
      //                 ],
      //               ),
      //             )
      //           ],
      //         ),
      //
      //         // Right Tab bar icons
      //
      //         Row(
      //           crossAxisAlignment: CrossAxisAlignment.start,
      //           children: <Widget>[
      //             MaterialButton(
      //               minWidth: 40,
      //               onPressed: () {
      //                 setState(() {
      //                   currentScreen =
      //                       NotificationPage(); // if user taps on this dashboard tab will be active
      //                   currentTab = 2;
      //                 });
      //               },
      //               child: Column(
      //                 mainAxisAlignment: MainAxisAlignment.center,
      //                 children: <Widget>[
      //                   Icon(
      //                     Icons.dashboard,
      //                     color: currentTab == 2 ? Colors.blue : Colors.grey,
      //                   ),
      //                   Text(
      //                     'Notifikasi',
      //                     style: TextStyle(
      //                       color: currentTab == 2 ? Colors.blue : Colors.grey,
      //                     ),
      //                   ),
      //                 ],
      //               ),
      //             ),
      //             MaterialButton(
      //               minWidth: 40,
      //               onPressed: () {
      //                 setState(() {
      //                   currentScreen =
      //                       AccountPage(); // if user taps on this dashboard tab will be active
      //                   currentTab = 3;
      //                 });
      //               },
      //               child: Column(
      //                 mainAxisAlignment: MainAxisAlignment.center,
      //                 children: <Widget>[
      //                   Icon(
      //                     Icons.chat,
      //                     color: currentTab == 3 ? Colors.blue : Colors.grey,
      //                   ),
      //                   Text(
      //                     'Akun',
      //                     style: TextStyle(
      //                       color: currentTab == 3 ? Colors.blue : Colors.grey,
      //                     ),
      //                   ),
      //                 ],
      //               ),
      //             )
      //           ],
      //         )
      //
      //       ],
      //     ),
      //   ),
      // ),
    );
  }
}
