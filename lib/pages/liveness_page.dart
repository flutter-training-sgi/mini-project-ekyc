import 'package:flutter/material.dart';

import 'package:camera/camera.dart';

class LivenessPage extends StatefulWidget {
  LivenessPage(Key key, this.title) : super(key: key);

  final String title;

  @override
  _LivenessPageState createState() => _LivenessPageState();
}

class _LivenessPageState extends State<LivenessPage> {
  CameraLensDirection cameraLensDirection = CameraLensDirection.front;

  List<String> _clasificationStatus = [
    'toleh kanan',
    'toleh kiri',
    'mengangguk'
  ];

  int _status = 0;



  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Stack(
        children: [
          Positioned(
            child: Align(
                alignment: Alignment.bottomCenter,
                child: Text("${_clasificationStatus[_status]}")
            ),
          ),
          SizedBox(
            height: 500,
            width: MediaQuery.of(context).size.width,
            child: null //Replace with camera
            ),
        ],
      ),
    );
  }
}

class FaceDetectorPainter extends CustomPainter {
  FaceDetectorPainter(this.imageSize, {this.reflection = false});

  final bool reflection;
  final Size imageSize;
  @override
  void paint(Canvas canvas, Size size) {
    final Paint paint = Paint()
      ..style = PaintingStyle.stroke
      ..strokeWidth = 2.0
      ..color = Colors.red;

    /*for (Face face in faces) {
      final faceRect =
      _reflectionRect(reflection, face.boundingBox, imageSize.width);
      canvas.drawRect(
        _scaleRect(
          rect: faceRect,
          imageSize: imageSize,
          widgetSize: size,
        ),
        paint,
      );
    }*/
  }

  @override
  bool shouldRepaint(FaceDetectorPainter oldDelegate) {
    return oldDelegate.imageSize != imageSize;
  }
}

Rect _reflectionRect(bool reflection, Rect boundingBox, double width) {
  if (!reflection) {
    return boundingBox;
  }
  final centerX = width / 2;
  final left = ((boundingBox.left - centerX) * -1) + centerX;
  final right = ((boundingBox.right - centerX) * -1) + centerX;
  return Rect.fromLTRB(left, boundingBox.top, right, boundingBox.bottom);
}

Rect _scaleRect({
  required Rect rect,
  required Size imageSize,
  required Size widgetSize,
}) {
  final scaleX = widgetSize.width / imageSize.width;
  final scaleY = widgetSize.height / imageSize.height;

  final scaledRect = Rect.fromLTRB(
    rect.left.toDouble() * scaleX,
    rect.top.toDouble() * scaleY,
    rect.right.toDouble() * scaleX,
    rect.bottom.toDouble() * scaleY,
  );
  return scaledRect;
}