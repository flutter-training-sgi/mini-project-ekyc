import 'dart:convert';
import 'dart:io';

import 'package:camera/camera.dart';
import 'package:dio/dio.dart';
import 'package:e_kyc_mini_project/models/faceRecognizeAddModel.dart';
import 'package:e_kyc_mini_project/models/userBenefModels.dart';
import 'package:e_kyc_mini_project/pages/success_page.dart';
import 'package:e_kyc_mini_project/widget/AppBar.dart';
import 'package:e_kyc_mini_project/widget/numericKeypad.dart';
import 'package:e_kyc_mini_project/widget/pinObsure.dart';
import 'package:e_kyc_mini_project/widget/stepper.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';

class FormRegisterStep extends StatefulWidget {
  UserBenefModel userBenefModel;

  FormRegisterStep(this.userBenefModel);

  @override
  _FormRegisterStepState createState() => _FormRegisterStepState();
}

class _FormRegisterStepState extends State<FormRegisterStep>
    with WidgetsBindingObserver {
  int currentStep = 0;
  String text = '';
  bool _isDone = false;
  var _pictureKey;

  CameraLensDirection cameraLensDirection = CameraLensDirection.front;

  List<String> _clasificationStatus = [
    'toleh kanan',
    'toleh kiri',
    'mengangguk',
    'buka mata',
    'tutup mata'
  ];

  int _status = 0;
  List<String> _picture = [];
  String _tmpPin1 = '';
  String _tmpPin2 = '';
  @override
  void initState() {
    super.initState();
  }

  _onKeyboardTap(String value) {
    setState(() {
      text = text + value;
    });
  }

  setPinUser() async {
    Response response = await updatePin(widget.userBenefModel.id, _tmpPin2);
    print(jsonEncode(response.data));
    print(_picture.length);
    if (response.statusCode == 200) {
      for (var a = 0; a < _picture.length; a++) {
        uploadFile(File(_picture.elementAt(a)));
      }
    }
  }

  uploadFile(File file) async {
    FaceRecognizeAddModel faceRecognizeAddModel =
        await uploadFace(file, widget.userBenefModel.id);
    if (faceRecognizeAddModel != null) {
      Response response = await insertUserFace(
          faceRecognizeAddModel.resultAws.faceId,
          widget.userBenefModel.id,
          "INPUT_PIN");
    }
  }

  Widget cameraku(BuildContext context, double height) {
    return SizedBox(
      height: MediaQuery.of(context).size.height * height,
      width: MediaQuery.of(context).size.width,
      child: null //Replace with default camera
    );
  }

  Widget stepOne(BuildContext context) {
    return Column(
      children: [
        Container(
          padding: EdgeInsets.only(left: 10.0, right: 10.0, top: 5, bottom: 5),
          child: StepperCustom(
            index: currentStep,
          ),
        ),
        SizedBox(
          height: 15.0,
        ),
        cameraku(context, 0.32),
        PinObsure(
          textLength: text.length,
          length: 6,
        ),
        NumericKeyboard(
          onKeyboardTap: _onKeyboardTap,
          textColor: Color.fromRGBO(40, 41, 48, 1),
          rightButtonFn: () {
            setState(() {
              text = text.substring(0, text.length - 1);
            });
          },
          rightIcon: Icon(
            Icons.backspace,
            color: Colors.black,
          ),
          leftButtonFn: () async {
            final path = join(
              (await getTemporaryDirectory()).path,
              '${DateTime.now()}.png',
            );
            if (text.length == 6) {
              if (_tmpPin1 == '') {
                setState(() {
                  _tmpPin1 = text;
                  text = '';
                });
                _pictureKey.currentState.takePicture(path);
                _picture.add(path);
              } else {
                if (_tmpPin1 == text) {
                  _pictureKey.currentState.takePicture(path);
                  _picture.add(path);
                  print(jsonEncode(_picture));
                  setPinUser();
                  setState(() {
                    _tmpPin2 = text;
                    text = '';
                    currentStep = 1;
                  });
                } else {
                  setState(() {
                    _tmpPin1 = '';
                    _tmpPin1 = '';
                    text = '';
                  });
                  print("Pin tidak sama");
                  Fluttertoast.showToast(
                      msg: "Pin tidak sama",
                      toastLength: Toast.LENGTH_SHORT,
                      gravity: ToastGravity.TOP,
                      timeInSecForIosWeb: 1,
                      backgroundColor: Colors.red,
                      textColor: Colors.white,
                      fontSize: 16.0);
                }
              }
            }
          },
          leftIcon: Icon(
            Icons.check,
            color: Colors.black,
          ),
        ),
      ],
    );
  }

  Widget stepTwo(BuildContext context) {
    return Column(
      children: [
        Container(
          padding: EdgeInsets.only(left: 10.0, right: 10.0, top: 5, bottom: 5),
          child: StepperCustom(
            index: currentStep,
          ),
        ),
        SizedBox(
          height: 15.0,
        ),
        Container(
          child: Text("${_clasificationStatus[_status]}"),
        ),
        cameraku(context, 0.7)
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    final deviceRatio = size.width / size.height;
    return Scaffold(
      appBar: AppBarWidget(
        title: "Setor Muka",
      ),
      body: Container(
        child: currentStep == 0
            ? stepOne(context)
            : currentStep == 1 ? stepTwo(context) : Container(),
      ),
    );
  }
}

class FaceDetectorPainter extends CustomPainter {
  FaceDetectorPainter(this.imageSize, {this.reflection = false});

  final bool reflection;
  final Size imageSize;

  @override
  void paint(Canvas canvas, Size size) {
    final Paint paint = Paint()
      ..style = PaintingStyle.stroke
      ..strokeWidth = 2.0
      ..color = Colors.red;

    /*for (Face face in faces) {
      final faceRect =
          _reflectionRect(reflection, face.boundingBox, imageSize.width);
      canvas.drawRect(
        _scaleRect(
          rect: faceRect,
          imageSize: imageSize,
          widgetSize: size,
        ),
        paint,
      );
    }*/
  }

  @override
  bool shouldRepaint(FaceDetectorPainter oldDelegate) {
    return oldDelegate.imageSize != imageSize;
  }
}

Rect _reflectionRect(bool reflection, Rect boundingBox, double width) {
  if (!reflection) {
    return boundingBox;
  }
  final centerX = width / 2;
  final left = ((boundingBox.left - centerX) * -1) + centerX;
  final right = ((boundingBox.right - centerX) * -1) + centerX;
  return Rect.fromLTRB(left, boundingBox.top, right, boundingBox.bottom);
}

Rect _scaleRect({
  required Rect rect,
  required Size imageSize,
  required Size widgetSize,
}) {
  final scaleX = widgetSize.width / imageSize.width;
  final scaleY = widgetSize.height / imageSize.height;

  final scaledRect = Rect.fromLTRB(
    rect.left.toDouble() * scaleX,
    rect.top.toDouble() * scaleY,
    rect.right.toDouble() * scaleX,
    rect.bottom.toDouble() * scaleY,
  );
  return scaledRect;
}
