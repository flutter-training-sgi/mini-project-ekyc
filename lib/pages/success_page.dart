import 'package:e_kyc_mini_project/models/userBenefModels.dart';
import 'package:e_kyc_mini_project/pages/home.dart';
import 'package:e_kyc_mini_project/pages/list_document.dart';
import 'package:flutter/material.dart';

class SuccessPage extends StatefulWidget {
  UserBenefModel userBenefModel;
  SuccessPage({this.userBenefModel});
  @override
  _SuccessPageState createState() => _SuccessPageState();
}

class _SuccessPageState extends State<SuccessPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        padding: EdgeInsets.only(top: 100.0, left: 10.0, right: 10.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Text(
              "Akun berhasil dibuat",
              style: TextStyle(fontSize: 16.0, fontWeight: FontWeight.bold),
            ),
            SizedBox(
              height: 100.0,
            ),
            Image.asset("assets/party.png"),
            SizedBox(
              height: 50.0,
            ),
            Text(
              "Selamat ${widget.userBenefModel.name}, Akun Anda telah berhasil dibuat dengan ID ${widget.userBenefModel.name}",
              style: TextStyle(fontSize: 16.0),
              textAlign: TextAlign.center,
            ),
            Spacer(),
            ButtonTheme(
              minWidth: MediaQuery.of(context).size.width * 0.9,
              child: ElevatedButton(
                style: ElevatedButton.styleFrom(
                  backgroundColor: Color.fromRGBO(35, 59, 123, 1),
                ),
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => ListDocument(
                              programId: "d77c4e81-e77e-41e0-bb17-752856619916",
                              userId: "${widget.userBenefModel.id}",
                            )),
                  );
                },
                child: Text(
                  "Isi data Step 2",
                  style: TextStyle(color: Colors.white),
                ),
              ),
            ),
            ButtonTheme(
              minWidth: MediaQuery.of(context).size.width * 0.9,
              child: ElevatedButton(
                style: ElevatedButton.styleFrom(
                  backgroundColor: Colors.white,
                ),
                onPressed: () {
                  Navigator.of(context).pushAndRemoveUntil(
                      MaterialPageRoute(builder: (ctx) => Home()),
                      (Route<dynamic> route) => false);
                },
                child: Text(
                  "Selesai",
                  style: TextStyle(color: Colors.black),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
