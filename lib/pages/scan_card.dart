import 'dart:io';

import 'package:camera_camera/camera_camera.dart';
import 'package:e_kyc_mini_project/models/KTPModels.dart';
import 'package:e_kyc_mini_project/pages/form_ktp.dart';
import 'package:e_kyc_mini_project/widget/AppBar.dart';
import 'package:e_kyc_mini_project/widget/customLoading.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';

class ScanCard extends StatefulWidget {
  final String documentName;
  final String userId;
  final String programId;

  ScanCard(this.documentName, this.userId, this.programId);

  @override
  _ScanCardState createState() => _ScanCardState();
}

class _ScanCardState extends State<ScanCard> {
  late KtpModels ktpModels;

  recognizeKTPs(File file) async {
    showLoadingDialog(context, "Processing...");
    ktpModels = await recognizeKTP(file);
    if (ktpModels != null) {
      Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => formKTP(
                  ktpModels,
                  widget.userId,
                  widget.programId
                )),
      );
    } else {
      Navigator.pop(context);
      Fluttertoast.showToast(msg: "Recognize KTP gagal");
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBarWidget(
        title: "${widget.documentName}",
      ),
      body: Camera(
        mode: CameraMode.fullscreen,
        initialCamera: CameraSide.back,
        enableCameraChange: false,
        //  orientationEnablePhoto: CameraOrientation.landscape,
        onChangeCamera: (direction, _) {
          print('--------------');
          print('$direction');
          print('--------------');
        },
        onFile: (file) {
          print(file);
          recognizeKTPs(file);
        },

        // imageMask: CameraFocus.rectangle(
        //   color: Colors.black.withOpacity(0.5),
        // ),
      ),
    );
  }
}
