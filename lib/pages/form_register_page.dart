import 'package:dio/dio.dart';
import 'package:e_kyc_mini_project/models/userBenefModels.dart';
import 'package:e_kyc_mini_project/pages/form_step_register.dart';
import 'package:e_kyc_mini_project/widget/AppBar.dart';
import 'package:e_kyc_mini_project/widget/customLoading.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:masked_text_input_formatter/masked_text_input_formatter.dart';

class FormRegisterPage extends StatefulWidget {
  @override
  _FormRegisterPageState createState() => _FormRegisterPageState();
}

class _FormRegisterPageState extends State<FormRegisterPage> {
  int? _radioValue = 0;
  List<bool> isSelected = [true, false];
  bool isDatePicker = false;
  TextEditingController _dateController = TextEditingController();
  TextEditingController _nameController = TextEditingController();
  final _formKey = GlobalKey<FormState>();

  String _selectedDate = 'Tap to select date';

  late UserBenefModel userBenefModel;

  void _handleRadioValueChange(int? value) {
    setState(() {
      _radioValue = value;
    });
  }

  @override
  void initState() {
    // _dateController.text = new DateFormat('yyyy-MM-dd').format(DateTime.now());
    super.initState();
  }

  Future<void> _selectDate(BuildContext context) async {
    final DateTime? d = await showDatePicker(
      //we wait for the dialog to return
      context: context,
      initialDate: DateTime.now(),
      firstDate: DateTime(1990),
      lastDate: DateTime(DateTime.now().year + 1),
    );

    if (d != null) //if the user has selected a date
      setState(() {
        // we format the selected date and assign it to the state variable
        _selectedDate = new DateFormat.yMMMMd("en_US").format(d);
        _dateController.text = new DateFormat('yyyy-MM-dd').format(d);
      });
  }

  submitData() async {
    Response response = await registerUserBenef(
        _nameController.text,
        _radioValue == 0 ? "l" : _radioValue == 1 ? "p" : "o",
        !isDatePicker
            ? _dateController.text
            : "${(DateTime.now().year - int.parse(_dateController.text))}-01-01");

    setState(() {
      userBenefModel = UserBenefModel.fromJson(response.data);
    });

    Navigator.push(
      context,
      MaterialPageRoute(
          builder: (context) => FormRegisterStep(userBenefModel)),
    );
  }

  void displayConfirmationDialog(BuildContext context) {
    showModalBottomSheet(
        context: context,
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.only(
          topLeft: Radius.circular(10.0),
          topRight: Radius.circular(10.0),
        )),
        backgroundColor: Colors.white,
        builder: (ctx) {
          return Container(
            height: MediaQuery.of(context).size.height * 0.6,
            padding:
                EdgeInsets.only(left: 20, right: 20.0, top: 50.0, bottom: 50.0),
            decoration: BoxDecoration(
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(10.0),
                    topRight: Radius.circular(10.0))),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Text(
                  "Apakah data sudah benar?",
                  style: TextStyle(fontSize: 16.0, fontWeight: FontWeight.bold),
                ),
                SizedBox(
                  height: 10.0,
                ),
                Text(
                  "Selanjutnya adalah sesi beneficary, berikan hp kamu dan dengan arahan kepada beneficiary ke arah wajah beneficiary sembari mengisi PIN",
                  textAlign: TextAlign.center,
                ),
                Spacer(),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    ButtonTheme(
                      // minWidth: 200.0,
                      height: 50.0,
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.all(Radius.circular(5.0))),
                      buttonColor: Color.fromRGBO(255, 193, 7, 1),
                      child: ElevatedButton(
                        onPressed: () {
                          Navigator.pop(context);
                        },
                        child: Text(
                          "Ubah Data",
                          style: TextStyle(fontWeight: FontWeight.w500),
                        ),
                      ),
                    ),
                    SizedBox(
                      width: 10.0,
                    ),
                    ButtonTheme(
                      // minWidth: 200.0,
                      height: 50.0,
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.all(Radius.circular(5.0))),
                      buttonColor: Color.fromRGBO(35, 59, 123, 1),
                      child: ElevatedButton(
                        onPressed: () {
                          showLoadingDialog(context, "Processing...");

                          submitData();
                        },
                        child: Text(
                          "Konfirmasi",
                          style: TextStyle(
                              fontWeight: FontWeight.w500, color: Colors.white),
                        ),
                      ),
                    ),
                  ],
                )
              ],
            ),
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBarWidget(
        title: "Daftarkan Calon Beneficiary",
      ),
      body: SingleChildScrollView(
        child: Container(
          padding: EdgeInsets.all(10.0),
          child: Form(
            key: _formKey,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  child: Text(
                    "Nama Lengkap",
                    style: TextStyle(fontSize: 14.0),
                  ),
                ),
                SizedBox(
                  height: 5,
                ),
                new TextFormField(
                  controller: _nameController,
                  // decoration: new InputDecoration(
                  //   labelText: "Enter Email",
                  //   fillColor: Colors.white,
                  //   border: new OutlineInputBorder(
                  //     borderRadius: new BorderRadius.circular(5.0),
                  //     borderSide: new BorderSide(),
                  //   ),
                  //   //fillColor: Colors.green
                  // ),
                  validator: (val) {
                    if (val?.length == 0) {
                      return "Nama tidak boleh kosong";
                    } else {
                      return null;
                    }
                  },
                  keyboardType: TextInputType.emailAddress,
                  style: new TextStyle(
                    fontFamily: "Poppins",
                  ),
                ),
                Container(
                  padding: EdgeInsets.only(top: 20.0),
                  child: Text(
                    "Gender",
                    style: TextStyle(fontSize: 14.0),
                  ),
                ),
                new Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    new Radio(
                      value: 0,
                      groupValue: _radioValue,
                      onChanged: _handleRadioValueChange,
                    ),
                    new Text(
                      'Pria',
                      style: new TextStyle(fontSize: 16.0),
                    ),
                    new Radio(
                      value: 1,
                      groupValue: _radioValue,
                      onChanged: _handleRadioValueChange,
                    ),
                    new Text(
                      'Wanita',
                      style: new TextStyle(
                        fontSize: 16.0,
                      ),
                    ),
                    new Radio(
                      value: 2,
                      groupValue: _radioValue,
                      onChanged: _handleRadioValueChange,
                    ),
                    new Text(
                      'Lainnya',
                      style: new TextStyle(fontSize: 16.0),
                    ),
                  ],
                ),
                SizedBox(
                  height: 10.0,
                ),
                Row(
                  children: [
                    Text("Tanggal Lahir"),
                    SizedBox(
                      width: 10.0,
                    ),
                    ToggleButtons(
                      constraints:
                          BoxConstraints(minWidth: 25.0, minHeight: 25.0),
                      borderColor: Colors.black,
                      fillColor: Color.fromRGBO(35, 59, 123, 1),
                      // borderWidth: 1,
                      selectedBorderColor: Colors.black,
                      selectedColor: Colors.white,
                      borderRadius: BorderRadius.circular(2.0),
                      children: <Widget>[
                        Container(
                          width: 10,
                          height: 5,
                        ),
                        Container(
                          width: 10,
                          height: 5,
                        ),
                      ],
                      onPressed: (int index) {
                        setState(() {
                          for (int i = 0; i < isSelected.length; i++) {
                            isSelected[i] = i == index;
                            isDatePicker = isSelected[i];
                            if (isDatePicker) {
                              _dateController.text = "20";
                            } else {
                              _dateController.text = DateFormat('yyyy-mm-dd')
                                  .format(DateTime.now());
                            }
                          }
                        });
                      },
                      isSelected: isSelected,
                    ),
                    SizedBox(
                      width: 10.0,
                    ),
                    Text("Usia")
                  ],
                ),
                SizedBox(
                  height: 15,
                ),
                TextFormField(
                  controller: _dateController,
                  inputFormatters: [
                    MaskedTextInputFormatter(
                      mask: 'DD-MM-YYYY',
                      separator: '-',
                    ),
                  ],
                  validator: (val) {
                    if (val?.length == 0) {
                      return "Tanggal Lahir tidak boleh kosong";
                    } else {
                      return null;
                    }
                  },
                  keyboardType: TextInputType.number,
                  // readOnly: !isDatePicker,
                  decoration: InputDecoration(
                    // fillColor: Colors.white,
                    // border: new OutlineInputBorder(
                    //   borderRadius: new BorderRadius.circular(5.0),
                    //   borderSide: new BorderSide(),
                    // ),
                    hintText: "dd-mm-yyyy",
                    suffixIcon: !isDatePicker
                        ? IconButton(
                            onPressed: () {
                              _selectDate(context);
                            },
                            icon: Icon(Icons.date_range),
                          )
                        : null,
                  ),
                ),
                SizedBox(
                  height: 30,
                ),
                Container(
                  decoration:
                      BoxDecoration(color: Color.fromRGBO(242, 242, 242, 1)),
                  height: 100,
                  padding: EdgeInsets.all(20.0),
                  child: Text(
                    "Informasi ini adalah informasi dasar agar para penerima beneficiary terdaftar ke dalam system duithape",
                    style: TextStyle(fontSize: 16.0),
                  ),
                )
              ],
            ),
          ),
        ),
      ),
      bottomNavigationBar: BottomAppBar(
        color: Colors.transparent,
        child: Padding(
          padding: EdgeInsets.all(10.0),
          child: ButtonTheme(
            buttonColor: Color.fromRGBO(35, 59, 123, 1),
            minWidth: 200.0,
            height: 50.0,
            child: ElevatedButton(
              onPressed: () {
                if (_formKey.currentState!.validate()) {
                  displayConfirmationDialog(context);
                }
              },
              child: Text(
                "Selanjutnya",
                style: TextStyle(color: Colors.white),
              ),
            ),
          ),
        ),
        elevation: 0,
      ),
    );
  }
}
