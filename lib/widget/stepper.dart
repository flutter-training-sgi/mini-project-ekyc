import 'package:flutter/material.dart';

class StepperCustom extends StatelessWidget {
  final int index;
  StepperCustom({this.index});

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: 10),
      // color: BLUE_LIGHT,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.end,
        children: [
          Column(
            children: [
              Text("Step 1", style: TextStyle(color:
              index == 0 ? Colors.black : Colors.grey.withOpacity(.7)),),
              Container(
                margin: EdgeInsets.only(top: 5.0),
                width: 10,
                height: 10,
                decoration: BoxDecoration(
                    color: index == 0 ? Colors.black : Colors.grey.withOpacity(.5),
                    borderRadius: BorderRadius.all(Radius.circular(5.0))),
              ),
            ],
          ),
          SizedBox(
            width: 10.0,
          ),
          Expanded(
              child: Divider(
            color: Colors.black,
          )),
          SizedBox(
            width: 10.0,
          ),
          Column(
            children: [
              Text(
                "Step 2",
                style: TextStyle(
                    color: index == 1
                        ? Colors.black
                        : Colors.grey.withOpacity(.7)),
              ),
              Container(
                margin: EdgeInsets.only(top: 5.0),
                width: 10,
                height: 10,
                decoration: BoxDecoration(
                    color:
                        index == 1 ? Colors.black : Colors.grey.withOpacity(.5),
                    borderRadius: BorderRadius.all(Radius.circular(5.0))),
              ),
            ],
          ),
          SizedBox(
            width: 10.0,
          ),
          Expanded(
              child: Divider(
            color: Colors.black,
          )),
          SizedBox(
            width: 10.0,
          ),
          Column(
            children: [
              Text(
                "Step 3",
                style: TextStyle(
                  color:
                      index == 2 ? Colors.black : Colors.grey.withOpacity(.5),
                ),
              ),
              Container(
                margin: EdgeInsets.only(top: 5.0),
                width: 10,
                height: 10,
                decoration: BoxDecoration(
                    color:
                        index == 2 ? Colors.black : Colors.grey.withOpacity(.7),
                    borderRadius: BorderRadius.all(Radius.circular(5.0))),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
