import 'package:flutter/material.dart';
class PinObsure extends StatelessWidget {
  final int textLength;
  final int length;
  PinObsure({this.textLength, this.length});

  bullet(){
    List<Widget> _tmp = [];
    for(var a = 0; a < length; a++){
      _tmp.add(Container(
        height: 42.0,
        width: 42.0,
        margin: EdgeInsets.only(right: 5.0),
        alignment: Alignment.center,
        decoration: BoxDecoration(
            border: Border.all(color: Color.fromRGBO(134,136,148,0.2)),
          borderRadius: BorderRadius.all(Radius.circular(3.0))
        ),
        child: Text(a >= textLength ? "" : "●", style: TextStyle(fontSize: 30, color: Colors.black, fontWeight: FontWeight.bold),),
      ));
    }

    return _tmp;
  }

  
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: 20.0),
      width: MediaQuery.of(context).size.width,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: bullet(),
      ),
    );
  }
}
