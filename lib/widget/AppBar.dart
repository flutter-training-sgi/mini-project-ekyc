import 'package:flutter/material.dart';

class AppBarWidget extends StatelessWidget implements PreferredSizeWidget {
  final String? title;
  AppBarWidget({Key key, this.title}) : preferredSize = Size.fromHeight(kToolbarHeight), super(key: key);

  @override
  final Size preferredSize;

  @override
  Widget build(BuildContext context) {
    return AppBar(
      leading: InkWell(
        onTap: (){
          Navigator.pop(context);
        },
        child: Icon(Icons.arrow_back_ios, color: Colors.black,),
      ),
      backgroundColor: Colors.white,
      title: Text("${title}", style: TextStyle(color: Colors.black),),
      elevation: 0.0,
    );
  }
}
